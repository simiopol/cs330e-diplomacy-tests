from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy (TestCase):
    # --------------
    # diplomacy_read
    # --------------
    
    def test_diplomacy_read_0(self):
        r = None
        commands = diplomacy_read(r)
        self.assertEqual(commands, [])
    
    def test_diplomacy_read_1(self):
        r = ["A Madrid Hold"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold")])
    
    def test_diplomacy_read_2(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold"),
                                    ("B", "Barcelona", "Move", "Madrid"),
                                    ("C", "London", "Support", "B")])
        
    def test_diplomacy_read_3(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold"),
                                    ("B", "Barcelona", "Move", "Madrid")])
    
    def test_diplomacy_read_4(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold"),
                                    ("B", "Barcelona", "Move", "Madrid"),
                                    ("C", "London", "Support", "B"),
                                    ("D", "Austin", "Move", "London")])
    
    def test_diplomacy_read_5(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold"),
                                    ("B", "Barcelona", "Move", "Madrid"),
                                    ("C", "London", "Move", "Madrid")])
        
    def test_diplomacy_read_6(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Dublin Support D"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold"),
                                    ("B", "Barcelona", "Move", "Madrid"),
                                    ("C", "London", "Move", "Madrid"),
                                    ("D", "Paris", "Support", "B"),
                                    ("E", "Dublin", "Support", "D")])
        
    def test_diplomacy_read_7(self):
        r = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Dublin Support D", "F Dallas Move Paris"]
        commands = diplomacy_read(r)
        self.assertEqual(commands, [("A", "Madrid", "Hold"),
                                    ("B", "Barcelona", "Move", "Madrid"),
                                    ("C", "London", "Move", "Madrid"),
                                    ("D", "Paris", "Support", "B"),
                                    ("E", "Dublin", "Support", "D"),
                                    ("F", "Dallas", "Move", "Paris")])

    # --------------
    # diplomacy_eval
    # --------------
    
    def test_diplomacy_eval_1(self):
        v = diplomacy_eval([("A", "Madrid", "Hold")])
        self.assertEqual(v, {"A": "Madrid"})
        
    def test_diplomacy_eval_2(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"),
                            ("B", "Barcelona", "Move", "Madrid"),
                            ("C", "London", "Support", "B")])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "Madrid",
                             "C": "London"})
        
    def test_diplomacy_eval_3(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"),
                            ("B", "Barcelona", "Move", "Madrid")])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "[dead]"})
    
    # THIS DOES NOT WORK YET!!!!!!
    def test_diplomacy_eval_4(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"),
                            ("B", "Barcelona", "Move", "Madrid"),
                            ("C", "London", "Support", "B"),
                            ("D", "Austin", "Move", "London")])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "[dead]",
                             "C": "[dead]",
                             "D": "[dead]"})
    
    def test_diplomacy_eval_5(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"),
                            ("B", "Barcelona", "Move", "Madrid"),
                            ("C", "London", "Move", "Madrid")])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "[dead]",
                             "C": "[dead]"})
    
    def test_diplomacy_eval_6(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"),
                            ("B", "Barcelona", "Move", "Madrid"),
                            ("C", "London", "Move", "Madrid"),
                            ("D", "Paris", "Support", "B"),
                            ("E", "Dublin", "Support", "D")])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "Madrid",
                             "C": "[dead]",
                             "D": "Paris",
                             "E": "Dublin"})
    
    def test_diplomacy_eval_7(self):
        v = diplomacy_eval([("A", "Madrid", "Hold"),
                            ("B", "Barcelona", "Move", "Madrid"),
                            ("C", "London", "Move", "Madrid"),
                            ("D", "Paris", "Support", "B"),
                            ("E", "Dublin", "Support", "D"),
                            ("F", "Dallas", "Move", "Paris")])
        self.assertEqual(v, {"A": "[dead]",
                             "B": "[dead]",
                             "C": "[dead]",
                             "D": "Paris",
                             "E": "Dublin",
                             "F": "[dead]"})
    
    # ---------------
    # diplomacy_print
    # ---------------

    def test_diplomacy_print_1(self):
        l = {"A": "Madrid"}
        v = diplomacy_print(l)
        self.assertEqual(v, "A Madrid\n")
    
    def test_diplomacy_print_2(self):
        l = {"A": "[dead]",
             "B": "Madrid",
             "C": "London"}
        v = diplomacy_print(l)
        self.assertEqual(v, "A [dead]\nB Madrid\nC London\n")
    
    def test_diplomacy_print_3(self):
        l = {"A": "[dead]",
             "B": "[dead]"}
        v = diplomacy_print(l)
        self.assertEqual(v, "A [dead]\nB [dead]\n")
        
    def test_diplomacy_print_4(self):
        l = {"A": "[dead]",
             "B": "[dead]",
             "C": "[dead]",
             "D": "[dead]"}
        v = diplomacy_print(l)
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_diplomacy_print_5(self):
        l = {"A": "[dead]",
             "B": "[dead]",
             "C": "[dead]"}
        v = diplomacy_print(l)
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\n")
        
    # ---------------
    # diplomacy_solve
    # ---------------
    
    def test_diplomacy_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_diplomacy_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_diplomacy_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_diplomacy_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_diplomacy_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

if __name__ == "__main__":
    main()