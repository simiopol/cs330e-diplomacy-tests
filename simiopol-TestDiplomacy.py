from io import StringIO
from unittest import main, TestCase

# from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve
from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support B\nE Austin Support "
                     "A\nF Tokyo Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\nE Austin\nF Tokyo\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()
