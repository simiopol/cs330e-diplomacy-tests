#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_diplomacy_solve_1(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support A\nC Berlin Move Madrid\nD London Move Barcelona\n")
        w = StringIO()
        diplomacy_read(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid A\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\n")
        w = StringIO()
        diplomacy_read(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")

    def test_diplomacy_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Dallas Move Paris\n")
        w = StringIO()
        diplomacy_read(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]\n")

    def test_diplomacy_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Berlin Support A\nD London Support C\nE Dublin Move Berlin\n")
        w = StringIO()
        diplomacy_read(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Berlin\nD London\nE [dead]\n")

    def test_diplomacy_solve_5(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support A\nC Berlin Move Madrid\n")
        w = StringIO()
        diplomacy_read(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
