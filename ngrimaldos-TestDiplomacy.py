#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        i = diplomacy_read(s)
        self.assertEqual(i, "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")

    def test_read_2(self):
        s = StringIO("B Barcelona Move Madrid\n")
        i = diplomacy_read(s)
        self.assertEqual(i, "B Barcelona Move Madrid")

    def test_read_3(self):
        s = StringIO("C London Support B\n")
        i = diplomacy_read(s)
        self.assertEqual(i, "C London Support B")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D")
        self.assertEqual(v[0][0] + " " + v[0][1], "A [dead]")
        self.assertEqual(v[1][0] + " " + v[1][1], "B Madrid")
        self.assertEqual(v[2][0] + " " + v[2][1], "C [dead]")
        self.assertEqual(v[3][0] + " " + v[3][1], "D Paris")
        self.assertEqual(v[4][0] + " " + v[4][1], "E Dublin")

    def test_eval_2(self):
        v = diplomacy_eval("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move Barcelona")
        self.assertEqual(v[0][0] + " " + v[0][1], "A [dead]")
        self.assertEqual(v[1][0] + " " + v[1][1], "B Madrid")
        self.assertEqual(v[2][0] + " " + v[2][1], "C London")
        self.assertEqual(v[3][0] + " " + v[3][1], "D Barcelona")

    def test_eval_3(self):
        v = diplomacy_eval(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF "
            "Dallas Move Paris")
        self.assertEqual(v[0][0] + " " + v[0][1], "A [dead]")
        self.assertEqual(v[1][0] + " " + v[1][1], "B [dead]")
        self.assertEqual(v[2][0] + " " + v[2][1], "C [dead]")
        self.assertEqual(v[3][0] + " " + v[3][1], "D Paris")
        self.assertEqual(v[4][0] + " " + v[4][1], "E Dublin")
        self.assertEqual(v[5][0] + " " + v[5][1], "F [dead]")
    
    def test_eval_4(self):
        v = diplomacy_eval(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF "
            "Dallas Move Paris\nG Austin Move Dublin")
        self.assertEqual(v[0][0] + " " + v[0][1], "A [dead]")
        self.assertEqual(v[1][0] + " " + v[1][1], "B [dead]")
        self.assertEqual(v[2][0] + " " + v[2][1], "C [dead]")
        self.assertEqual(v[3][0] + " " + v[3][1], "D [dead]")
        self.assertEqual(v[4][0] + " " + v[4][1], "E [dead]")
        self.assertEqual(v[5][0] + " " + v[5][1], "F [dead]")
        self.assertEqual(v[6][0] + " " + v[6][1], "G [dead]")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Madrid']])
        self.assertEqual(w.getvalue(), "A Madrid")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [['B', '[dead]']])
        self.assertEqual(w.getvalue(), "B [dead]")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [['C', '[dead]']])
        self.assertEqual(w.getvalue(), "C [dead]")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Barcelona")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\nF Dallas Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Dublin\nF [dead]")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
